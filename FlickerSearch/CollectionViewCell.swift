//
//  CollectionViewCell.swift
//  FlickerSearch
//
//  Created by Lyndy Tankersley on 3/1/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var cellImage: UIImageView!

    @IBOutlet var cellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
