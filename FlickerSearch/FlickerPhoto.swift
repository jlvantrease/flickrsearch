//
//  FlickerPhoto.swift
//  FlickerSearch
//
//  Created by Lyndy Tankersley on 3/3/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit

class FlickerPhoto{
    //var url : URL
    var photoID : String
    var farm : String
    var server : String
    var secret : String
    var title : String
    
    init (photoID:String, farm:String, server:String, secret:String, title:String){
        self.photoID = photoID
        self.farm = farm
        self.server = server
        self.secret = secret
        self.title = title
     //   self.url = URL(string: "https://farm\(farm).saticflickr.com/\(server)/\(photoID)_\(secret)\\100.jpg")!
    }
    
    
    
}
