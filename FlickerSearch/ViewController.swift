//
//  ViewController.swift
//  FlickrSearch
//
//  Created by Lyndy Tankersley on 2/27/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    @IBOutlet var collectionView: UICollectionView!
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    var arrayPhotos = [FlickerPhoto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil),forCellWithReuseIdentifier: "cell")
        //self.getPhotos(completionHandler: "nil")
    
        let url = urlForSearch(_searchTerm: "computer")
        
         
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in guard error == nil else{
                print(error!)
                return
            }
            guard let data = data else{
                print("No data")
                return
            }
            
            let resultsDict = try! JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            let photosJson = resultsDict!["photos"] as? [String: AnyObject]
         
            let photoDict = photosJson!["photo"] as? [[String: AnyObject]]
    
            
            for photo in photoDict!{
                let photoId = photo["id"] as? String
                let farm = photo["farm"] as? String
                let server = photo["server"] as? String
                let secret = photo["secret"] as? String
                let title = photo["title"] as? String
            
                
                let photoObect = FlickerPhoto(photoID: photoId ?? "", farm: farm ?? "", server: server ?? "", secret: secret ?? "", title: title ?? "")
                self.arrayPhotos.append(photoObect)
                //print(self.arrayPhotos[0].title)
                }
        
        }
        
        task.resume()
        
        //print(self.arrayPhotos[0].title)
  
    }



    fileprivate func urlForSearch(_searchTerm:String) -> URL? {
        guard let escapedTerm = _searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)else{
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=25&format=json&nojsoncallback=1"
        
        guard  let url = URL(string:URLString) else {
            return nil
        }
        return url
    }
    /*
    func createURLArray(photoDict: Dictionary<String,AnyObject>) -> Array<Any>{
        
        for photoObject in photoDict{
            
        }
        
    }
    */
    
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.cellImage.backgroundColor = UIColor.black
        
        //let cellIndex = searches[(indexPath as NSIndexPath).section].searchResults[(indexPath as NSIndexPath).row]
        //let photoUrl = arrayPhotos[0].url
        //print(arrayPhotos[0].photoID)
        //let data = try! Data(contentsOf: photoUrl)
        //cell.cellImage.image = UIImage(data: data)
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 40, height: self.view.frame.size.width/2-40)
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be arecreated.
    }
    
}

